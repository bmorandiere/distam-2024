# Distam - 2024
## Atelier Tropy - jeudi 11 juillet 2024 

### Le Plugin Tropy-Nakala

Le plugin permet de d'exporter les ressources de [Tropy](https://tropy.org) vers [Nakala](https://documentation.huma-num.fr/nakala/), crée un Manifest IIIF présentation puis exporte ces manifest vers [Omeka S](https://omeka.org/s/)


#### Installation

Téléchargez le fichier `.zip`, nommé [Plugin-Nakala](https://gitlab.huma-num.fr/bmorandiere/distam-2024/-/raw/main/tropy-nakala.zip). Dans Tropy, naviguez vers *Préférences... > Plugins* et cliquez sur *Installer le plugin* pour sélectionner le fichier ZIP téléchargé.

#### Configuration du plugin

Pour configurer le plugin, cliquez sur son bouton *Settings* dans *Preferences > Plugins* :

Certains paramètres s'appliquent à la fois à l'importation et à l'exportation :

- Choisissez un *Nom* de plugin qui apparaîtra dans le menu *Fichier > Exporter*.
- Renseigner l'URL de l'instance Nakala (instance de test dans le cadre de cet atelier : test.nakala.fr)
- Renseigner la clé d'API nakala. Cette clé peut être trouvée / générée dans son compte :  https://nakala.fr/u/account . Dans le cadre de cet atelier : 

|   humanID   |   Mot de passe    |   Clé d'API    |  
|---    |:-:    |:-:    |
|   tnakala    |   IamTesting2020    |    01234567-89ab-cdef-0123-456789abcdef   |   
|   unakala1    |   IamTesting2020    |   33170cfe-f53c-550b-5fb6-4814ce981293    | 
|   unakala2   |   IamTesting2020     |   f41f5957-d396-3bb9-ce35-a4692773f636    |       
|   unakala3   |   IamTesting2020     |   aae99aba-476e-4ff2-2886-0aaf1bfa6fd2    |       

- Renseigner les identifiants des collections Nakala séparés (par le séparateur défini plus loin § par défaut). Les identifiants apparaissent dans l'entête de la collection et est du type : 10.34847/nkl.07b9psy8
- Choisissez si les donnnées seront publiques ou nnon dans Nakala
- Précisez le séparateurs des champs multivalués (§ par défaut)
- Choisissez le fichier (format csv) dans lequel les résultats exportés seront listés avec les métadonnées et les liens vers Nakla/ Omeka
- Remplissez l'*URL de l'API Omeka* pour votre instance d'Omeka S.
- Précisez si les donnnées seront publiques ou nnon dans Omeka
- Remplissez les champs *Clé d'identité* et *Clé d'authentification*, qui peuvent être trouvés dans Omeka S sous *Préférences utilisateur > Clés API*.
- Renseigner l'identifiant de la collections Omeka 
- Optionnellement, spécifiez le *Resource Template ID* du modèle de ressource Omeka à appliquer aux éléments exportés.
- Utilisez l'icône *+* à l'extrême droite pour créer de nouvelles instances de plugin (vous pouvez ainsi avoir plusieurs configurations en parallèle).

#### Utilisation

Sélectionnez les éléments à exporter, puis cliquez sur *File > Export > tropy-nakala* pour lancer l'exportation. Le plugin téléchargera les éléments sélectionnés dans un premier temps vers l'instance Nakala spécifiée, il générera un manisfest pour chaque élement et les exportera ensuite vers l'instance Nakala spécifiée.




